void SensorLuz(){
    int nivelDeLuz1 = analogRead(PIN_LUZ1);  // fotorresistor1
    nivelDeLuz1 = map(nivelDeLuz1, 50, 910, 0, 100); // max 940 min 100
    if(nivelDeLuz1 < 5){ // si el valor es muy bajo...
    nivelDeLuz1 = 0;    // ...lo hago 0 directamente
    }
    nivelDeLuz1 = constrain(nivelDeLuz1, 0, 100);
     dato = "";
     dato += nivelDeLuz1;
     int nivelDeLuz2 = analogRead(PIN_LUZ2);  // fotorresistor2
    nivelDeLuz2 = map(nivelDeLuz2, 50, 910, 0, 100); // max 940 min 100
    if(nivelDeLuz2 < 5){ // si el valor es muy bajo...
    nivelDeLuz2 = 0;    // ...lo hago 0 directamente
    }
    nivelDeLuz2 = constrain(nivelDeLuz2, 0, 100);
     dato1 = "";
     dato1 += nivelDeLuz2;
  }
  
  
void Temperatura_Ambiente(){
      float h = dht.readHumidity(); //Leemos la Humedad
      float t = dht.readTemperature(); //Leemos la temperatura en grados Celsius
      float f = dht.readTemperature(true); //Leemos la temperatura en grados Fahrenheit
   Serial.println(t);
        if (t > threshold) {
    if (alarma == 0) {
           digitalWrite(Extractor, LOW);
      alarma = 1;
      threshold = TempBase - histeresis;
       
    }
  } else {
    if (alarma == 1) {
      digitalWrite(Extractor, HIGH);
      alarma = 0;
      threshold = TempBase + histeresis;
    }
  }
  
 }

void AjusteTemp(){
  dht.begin();
  float t = dht.readTemperature();

  if (t > TempBase) {
    alarma = 1;
    threshold = TempBase - histeresis;
     digitalWrite(Extractor, LOW);
  }
  else {
    alarma = 0;
    threshold = TempBase + histeresis;
    digitalWrite(Extractor, HIGH);
  }
  }


void AjusteNivel(){
    pinMode(Trigger, OUTPUT); //pin como salida
  pinMode(Echo, INPUT);  //pin como entrada
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
}

void nivel(){
  
  long t; //timepo que demora en llegar el eco
  long d; //distancia en centimetros

  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          //Enviamos un pulso de 10us
  digitalWrite(Trigger, LOW);
  
  t = pulseIn(Echo, HIGH); //obtenemos el ancho del pulso
  d = t/59;             //escalamos el tiempo a una distancia en cm
  
  Serial.print(d);      //Enviamos serialmente el valor de la distancia
  Serial.print("cm");

}
    
  
  
