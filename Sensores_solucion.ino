// Control Relacional para la modificacion del ph ya que al modificarse el ph se modifica el suministro de los respecivos liquidos para calibrar el ph

void Sensor_Ph(){
    static unsigned long timepoint = millis();
    if(millis()-timepoint>1000U){                  //Tiempo de refresco entre datos de 1s
        timepoint = millis();
        temperature = readTemperature();         // linea para leer la temperatura proporcionada por el sensor para 
                                                   // Realizar una compensacion
        voltage = analogRead(PH_PIN)/1024.0*5000;  // leer voltaje tomado de la señal analogica
        phValue = ph.readPH(voltage,temperature);  // convertir el voltaje en unidad de ph con uso de la funcion ph.readPH
    }
    ph.calibration(voltage,temperature);           // linea de codigo para calibrar el sensor por medio del puerto CMD
      
}

void Sensor_CE(){
     static unsigned long timepoint1 = millis();
    if(millis()-timepoint1>1000U)  //time interval: 1s
    {
      timepoint1 = millis();
      voltage1 = analogRead(EC_PIN)/1024.0*5000;   // leer voltaje tomado de la señal analogica
      temperature = readTemperature();           // linea para leer la temperatura proporcionada por el sensor para 
                                                   // Realizar una compensacion
      ecValue =  ec.readEC(voltage1,temperature);  // convertir el voltaje en unidad de CE con uso de la funcion ec.readEC
      
    }
    ec.calibration(voltage1,temperature);   
}

void correccion_ph(){
    if ((phValue >=Sph_LOW)&& (phValue <=Sph_HIGH)){  
    digitalWrite(Bomba1, HIGH);
    digitalWrite(Bomba2, HIGH);
    estado=0;
    }
 
   if (phValue<Sph_LOW){
   digitalWrite(Bomba1, LOW);
   digitalWrite(Bomba2, HIGH);
   Serial.println("Se esta calibrando Apertura Valvula 1");
   
   }

   if (phValue>Sph_HIGH){
   digitalWrite(Bomba2, LOW);
   digitalWrite(Bomba1, HIGH);
   Serial.println("Se esta calibrando Apertura Valvula 2");
     estado=1;
   }
}

void correccion_ce(){
    if ((ecValue >=Spce_LOW)&& (ecValue <=Spce_HIGH)){
 digitalWrite(Bomba3, HIGH);
 }
 
 if (ecValue<Spce_LOW){
  digitalWrite(Bomba3, HIGH);
 }

 if (ecValue>Spce_HIGH){
 digitalWrite(Bomba3, LOW);
 }
}


float readTemperature()
{
    sensorDS18B20.requestTemperatures();
}
