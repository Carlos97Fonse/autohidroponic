//           "Automatizacion de un Cultivo Hidroponico NFT para la optimizacion de Hortalizas de Hoja en la casa rural
//                             VILLA FONSECA ubicada en el municipio de Salazar de las Palmas N.S"
//                                    Carlos Humberto Fonseca Sanchez  cod. 1090980
//                                    Ramon Andres Burgos Medina       cod.
//                              Director: Johny Omar Medina Codirecctor: Eloy Orjuela
//  Derechos de escritura correspondientes a Carlos Humberto Fonseca Sanchez. Prohibida su comercializacion o mal uso de terceros
/*


Definicion de las Librerias que se hacen uso dentro de este programa para la ejecucion del proyecto
este bloque se conforma de librerias para la correspondiente lectura de los sensores, visualisacion por parte de la lcd
escritura y sobreescritura de la memoria EEPROM, Lectura del teclado matricial, Reloj en tiempo real y por ultimo
Librerias que hacen posible el uso de protocolos de comunicacion como ModBus (Funcion Wire) o comunicacion parasita (Funcion OneWire)                 */

#include <StaticThreadController.h>
#include <Thread.h>            // Libreria Acondicionada para realizar la programacion MultiHilo
#include <ThreadController.h>  // Libreria Acondicionada para realizar la programacion MultiHilo
#include "DFRobot_PH.h"        // Libreria Sensor de PH
#include "DFRobot_EC.h"        // Libreria Sensor de Conductividad Electrica
#include <EEPROM.h>            // Libreria memoria EEPROM
#include <Wire.h>              // Libreria que permite comunicar con el arduino dispositivos con comunicacion I2C
#include <LiquidCrystal_I2C.h> // Libreria pantalla LCD
#include <Keypad.h>            // Libreria Teclado keypad
#include <DS3231.h>            // Libreria para el reloj en tiempo real
#include <DHT.h>               // Libreria para sensor de humedad y temperatura DHT22
#include <OneWire.h>           // Libreria para comunicar con el arduino dispositivos de forma parasita
#include <DallasTemperature.h> // Libreria correspondiente al sensor de temperatura DS18B20

/*Definicion de las posiciones donde se guardaran 
los datos en la memoria EEPROM del Arduino*/

#define address 5 
#define address2 10 
#define address3 15

/*Declaracion de los ControllThreads o Hilos de control 
Estos son bloques de control a los cuales se les asignan varias funciones de proceso 
con el fin de organizar las funciones por jerarquia o grupo de control de esta forma 
se logra simular la simultaneidad o la ejecucion de procesos en paralelo */

/*La estructura de este parrafo de codigo se compone por nombrar la funcion ThreadController
en consiguiente el nombre especifico para ese control y por ultimo asignar la funcion que va 
a realizar el mismo en este caso igualando dicho nombre con la jerarquia de CONTROLADOR dado por la funcion
ThreadController();                                                                                                                                */

ThreadController controll = ThreadController();
ThreadController controll2 = ThreadController();
ThreadController controll3 = ThreadController();

/*Threads o Hilos correspondientes al Control numero 1. Este control tiene como funcion
agrupar las funciones que intervienen en la solucion nutritiva estas son:
1. Lectura de Ph 2.Lectura de CE 3.Lectura de la temperatura de la solucion
Ademas realiza funciones de correccion estas son:
A.Correccion de Ph B. Correccion de CE C.Control de Riegos D.Visualisacion Menu en LCD                                                                */

Thread* Threadph = new Thread();    
Thread Threadce = Thread();
Thread Threadcorrph = Thread();
Thread Threadcorrce = Thread();
Thread Threadlcd =  Thread();
Thread ThreadTempsoluc = Thread();
Thread ThreadRiegos = Thread();
//Thread ThreadCalibracion = Thread();

/* Thread o Hilo correspondientes al Control numero 2 . Este control tiene como unica funcion
manipular la funcion encargada de enseñar en pantalla un menu de proceso con variables especificas 
del sistema cada que este se encuentre realizando la operacion de RIEGO. Esto se realiza con el fin de que el controlador no
sea manipulado (Pulsar en el teclado cualquier tecla) mientras realiza la funcion ya que generaria 
una interrupcion en el microcontrolador culminando el proceso de riego.                                                                              */

Thread* ThreadLCDBomba = new Thread();

/*Threads o Hilos correspondientes al Control numero 3. Este control tiene como funcion
agrupar las funciones que intervienen en las variables ambientales del proceso estas son:
1. Lectura de Temperatura 2.Lectura de Humedad 3.Lectura del Caudal 4. Lectura del Nivel 5. Luminocidad
Lo particular de este control es que la funcion encargada de Medir la Temperatura y Humedad
ademas realiza la correccion de la misma por medio de un control ON/OFF con histeresis aplicado al extractor de aire                                                             */

Thread* ThreadTemHumedad = new Thread();
Thread ThreadLuz =  Thread();
Thread ThreadCaudal =  Thread();
Thread Threadnivel =  Thread();


int outputValue;
int outputValue1;
int estado;
unsigned long lastmilis;
unsigned long lastmilis1;
unsigned long lastmilis2;
unsigned long lastmilis3;
String estad;


/* Bloque Dirijido a declarar las variables que interceden para el correcto funcionamiento
del reloj en tiempo real RTC DS3231 mod ZS-0.42 el cual consta con variables de minutos, segundos, horas y dias
esto con el fin de activar en el momento exacto el sistema de riego 
La variable Tiempo_riego hace uso a como el nombre lo indica el tiempo que dura la Electrobomba encendida realizando el riego.

En la primera fila de codigo se puede ver la siguiente estructura DS3231 rtc(SDA,SCL) estamos inicializando la comunicacion 
ModBus (Comunicacion a dos hilos SDA SCL y Comun) entre el Reloj y el Arduino Mega. Donde SDA es la señal Digital de datos y SCL 
es la Señal digital de Reloj de esta forma se envian y reciven datos entre el maestro (Arduino Mega) y el exclavo (DS3231) 
por solo estos dos cables. El Maestro Es el encargado de generar la señal SCL cabe resaltar que el numero de direccion es unico
entre dispositivos exclavos que esten conectados al Bus I2c*/

 DS3231 rtc(SDA,SCL);
 int r_diaSemana; 
 int segundo;
 int minuto;
 int hora;
 int tiempo_riego;
 Time T;

 /* El siguiente bloque debe habilitarse para reajustar el reloj RTC cargar el arreglo Eliminar la linea y volver a cargar si esta
  La linea fija la fecha, hora y dia de la semana, se debe suprimir la linea en la segunda carga
  Ejemplo 2018 noviembre 11, 08:00:00  dia 6-Sabado (0=Dom, 1=Lun, 2=Mar, 3=Mie, 4=Jue, 5=Vie, 6=Sab)
   DateTime dt(2018, 11, 14, 22, 12, 0, 3);
*/

/* Acontinuacion se declaran como variables enteras las horas minutos y segundos en el que la Electrobomba
se debe activar para dar inicio al proceso de riego donde se colocaron 10 riegos en un dia con un lapzo de 1 hora entre riego
y 15 minutos de riego lo que garantiza la correcta humidificacion en las raices del cultivo                                                      */

//Horario para ENCENDIDO de riego
 int h1_c1=6;   int m1_c1=15;   int s1_c1=0; // 6:15
 int h2_c1=7;   int m2_c1=30;   int s2_c1=0; // 7.30
 int h3_c1=8;   int m3_c1=45;   int s3_c1=0; // 8.45
 int h4_c1=10;  int m4_c1=0;    int s4_c1=0; // 10
 int h5_c1=11;  int m5_c1=15;   int s5_c1=0; // 11.15
 int h6_c1=12;  int m6_c1=20;   int s6_c1=0; // 12.30
 int h7_c1=13;  int m7_c1=45;   int s7_c1=0; // 13.45
 int h8_c1=15;  int m8_c1=0;    int s8_c1=0; // 15
 int h9_c1=16;  int m9_c1=15;   int s9_c1=0; // 16.15
 int h10_c1=17; int m10_c1=30;  int s10_c1=0;//17.30
 int h11_c1=99; int m11_c1=0;   int s11_c1=0; 
 int h12_c1=99; int m12_c1=0;   int s12_c1=0;
 int h13_c1=99; int m13_c1=0;   int s13_c1=0;
 int h14_c1=99; int m14_c1=0;   int s14_c1=0;
 int h15_c1=99; int m15_c1=0;   int s15_c1=0;
 int h16_c1=99; int m16_c1=0;   int s16_c1=0;

 //Horarios de APAGADO de Riego
 int h1_c3=6;   int m1_c3=30;   int s1_c3=0;   //6.30
 int h2_c3=7;   int m2_c3=45;   int s2_c3=0;   //7.45
 int h3_c3=9;   int m3_c3=0;    int s3_c3=0;    //9
 int h4_c3=10;  int m4_c3=15;   int s4_c3=0;   // 10.15
 int h5_c3=11;  int m5_c3=30;   int s5_c3=0;   // 11.30
 int h6_c3=12;  int m6_c3=45;   int s6_c3=0;   //12.45
 int h7_c3=14;  int m7_c3=0;    int s7_c3=0;    //14
 int h8_c3=15;  int m8_c3=15;   int s8_c3=0;   //15.15
 int h9_c3=16;  int m9_c3=30;   int s9_c3=0;   //16.30
 int h10_c3=17; int m10_c3=45;  int s10_c3=0;  //17.45
 int h11_c3=22; int m11_c3=22;  int s11_c3=0;
 int h12_c3=99; int m12_c3=0;   int s12_c3=0;
 int h13_c3=99; int m13_c3=0;   int s13_c3=0;
 int h14_c3=99; int m14_c3=0;   int s14_c3=0;
 int h15_c3=99; int m15_c3=0;   int s15_c3=0;
 int h16_c3=99; int m16_c3=0;   int s16_c3=0;

 //Horarios de calibracion de ph y ce
 int h1_c2=6;   int m1_c2=0;    int s1_c2=0;
 int h2_c2=8;   int m2_c2=45;   int s2_c2=0;
 int h3_c2=10;  int m3_c2=15;   int s3_c2=0;
 int h4_c2=12;  int m4_c2=45;   int s4_c2=0;
 int h5_c2=15;  int m5_c2=15;   int s5_c2=0;
 int h6_c2=17;  int m6_c2=45;   int s6_c2=0;
 int h7_c2=11;  int m7_c2=15;   int s7_c2=0;
 int h8_c2=99;  int m8_c2=0;    int s8_c2=0;
 int h9_c2=99;  int m9_c2=0;    int s9_c2=0;
 int h10_c2=99; int m10_c2=0;   int s10_c2=0;
 int h11_c2=99; int m11_c2=0;   int s11_c2=0;
 int h12_c2=99; int m12_c2=0;   int s12_c2=0;
 int h13_c2=99; int m13_c2=0;   int s13_c2=0;
 int h14_c2=99; int m14_c2=0;   int s14_c2=0;
 int h15_c2=99; int m15_c2=0;   int s15_c2=0;
 int h16_c2=99; int m16_c2=0;   int s16_c2=0;

///DEFINIR CONSTANTES SE INICIALIZAN LAS CONSTANTES EN 0////
int i=0;
int b=0;
int c=0;
int x =0;
int y =0;
int z =0;
int p =0;
int d;
int R=0;
int f=0;

int contador = 0;
int estadoanterior = 0;
int planta;
/* Variables que interceden en el proceso de control de la solucion nutritiva donde se utiliza un parametro alto y bajo
para asi determinar en que momento el ph es Acido (Ph < 7) o Base (Ph > 7). En el caso de la CE este rango determina
que momento la solucion esta en buen estado al determinar el porcentaje de sales que esta contiene. 
De esta forma se añade al codigo como entero Sph_LOW, Sph_HIGH etc donde S significa SetPoint 
estos enteros no tiene un valor especifico asigano ya que esos valores de SetPoint se veran modificados en el momento que el 
operario seleccione un cultivo en especifico.

En resumen estas variables se veran sobre escritas y modificadas segun el cultivo que la persona decee implementar                                   */

int Sph_LOW;
int Sph_HIGH;
int Spce_LOW;
int Spce_HIGH;
int SpTemp1;

/*Al igual que las variables anteriores solo que estas variables estan ligadas al proceso de control del ambiente Temperatura
humedad luminocidad*/

int SpTemp2_HIGH;
int SpTemp2_LOW;
int SpLx_HIGH;
int SpLx_LOW;
int SpQ;
int SpHumedad_LOW;
int SpHumedad_HIGH;
int nivelDeLuz1;
int nivelDeLuz2;
String dato; 
String dato1; 

//  ASIGNACION DE PINES SENSORES EXTERNOS (TEMPERATURA,HUMEDAD,CAUDAL,LUMINOCIDAD)
//     1. SENSOR DE HUMEDAD Y TEMPERATURA DHT22
/* Se asigna h y t Humedad y temperatura respectivamente, como datos flotantes (Float) ya que estos datos
contaran con decimales haciendo de la recoleccion de datos un tanto mas precisa. */

float h;                
float t;
#define DHTPIN 30             // Pin donde está conectado el sensor DHT22
#define DHTTYPE DHT22         // Se define la clase de Sensor en este caso es DHT22
DHT dht(DHTPIN, DHTTYPE);
long tiempoUltimaLectura=0;   //Para guardar el tiempo de la última lectura

//     2. SENSORES DE LUMINOCIDAD LDR 
/* Este sensor es de tipo analogico, ya que se ingresa una diferencia de potencial al pin de arduino 
generado por la divicion de tencion entre la resistencia variable luminica LDR y una resistencia base en este caso
de valor 1K teniendo en cuenta la suma de resistencia suministrada por los conductores */

#define PIN_LUZ1 A3            // Pin Analogico A3
#define PIN_LUZ2 A5            // Pin Analogico A5

//     3. VARIABLES CONTROL DE TEMPERATURA IMPLEMENTANDO UN CONTROL ON/OFF CON HISTERESIS
/*Donde TempBase hace alucion a la temperatura ideal de crecimiento de las plantas este valor se vera afecta al seleccionar el tipo
de cultivo, adicionalmente se asigna como constante el valor de 1 grado celcius en la histeresis con el fin de evitar multiples
activaciones por parte del actuador en este caso el extractor de aire                                                                               */

float TempBase;
float threshold;
const float histeresis = 1; 

//Se define a la variable alarma de tipo Boolean para manejar 2 estados logicos 1 (Alto ) 0 (Bajo) Valor de 1 = ALARMA, 0 = NO ALARMA 
boolean alarma;
//     4. SENSOR DE ULTRASONIDO HACIENDO LA FUNCION DE MEDIR EL NIVEL EN EL TANQUE  

const int Trigger = 12;   //Pin digital 2 para el Trigger del sensor
const int Echo = 13;      //Pin digital 3 para el Echo del sensor


//     ASIGNACION DE PINES ANALOGICOS. SENSORES ANALOGICOS (PH,CE,T)

#define PH_PIN A0 //PH
#define EC_PIN A1 //CE
#define Tsolucion_PIN A2 //T
const int pinDatosDQ = 9; // Pin donde se conecta el bus 1-Wire conectar sensor de temperatura
float voltage1,ecValue;
float voltage,phValue; 
DFRobot_PH ph;
DFRobot_EC ec;
OneWire oneWireObjeto(pinDatosDQ);
DallasTemperature sensorDS18B20(&oneWireObjeto);
float temperature = sensorDS18B20.getTempCByIndex(0);
//             Asiganacion de pines de salida
/* el siguiente bloque de codigo contiene la asignacion de pines de salida todos digitales ya que
solo se usan dos niveles logicos (True, False) encendido o apagado del respectivo interruptor 
(Rele de 5A a excepcion de la electrobomba Este posee un rele de 10A) en los diferentes actuadores 
que modifican las variables de proceso Ph, CE T y Humedad donde:
Bomba 1 = La Bomba Peristaltica 1 que se encarga de Subir el Ph cuando este se encuentra Acido
Bomba 2 = La Bomba Peristaltica 2 que se encarga de Bajar el Ph cuando este se encuentra Base 
Bomba 3 = Electrovalvula de solenoide 12v que se encarga de regular la CE cuando esta se encuentra con abudantes sales tiene el doble proposito
          de ademas cuando el deposito este vacio llenar el mismo de forma automatica              
Bomba 4 = Electrobomba de 1/2 HP la que cual suple la funcion de impulsar la solucion nutritiva a las camas del cultivo
Extractor = Extractor de aire Marca SIEMENS de 80w el cual cumple la funcion de mantener regulada la temperatura                                 */
          
const int Bomba1 = 32;  // Bomba 1 es para Subir ph
const int Bomba2 = 33;  // Bomba 2 es para bajar ph
const int Bomba3 = 31;  // Bomba 3 es para agregar agua (calibrar CE)
const int Bomba4 = 35;  // Bomba 4 es para encender motobomba
const int Extractor = 26;      // pin digital activacion rele del extractor


// Asignacion de la comunicacion Modbus entre la LCD 20x4 con salida I2C y el Arduino mega, Asignando la direccion 0x27 en el Bus I2C
LiquidCrystal_I2C lcd(0x27,20,4);  

unsigned long tAntes = 0;
unsigned long tAhora = 0;
unsigned long tEjecucion = 500;
unsigned long tAntes1 = 0;
unsigned long tAhora1 = 0;
unsigned long tEjecucion1 = 500;
unsigned long tAntes2 = 0;
unsigned long tAhora2 = 0;
unsigned long tEjecucion2 = 500;



//     Configuracion Teclado matricial 3x4 (3 Columnas - 4 filas)
/* Asignacion de las variables que implican la utilizacion del teclado matricial, donde el entero pausa es el retardo
que se le da a cada pulsacion de la tecla expresado en ms, se asigna de tipo char "Tecla" ya que es un caracter y no hace 
uso en alguna funcion u operacion, lo siguiente es definir la cantidad de filas (ROWS=4) y la cantidad de columnas (COLS=3) 
luego se realiza una matriz asignando la posicion de cada caracter con respecto al teclado en fisico por ultimo
se definen los pines digitales correspondientes a las filas y a las columnas                                                                     */

int pausa = 10;
char tecla;
const byte ROWS = 4;
const byte COLS = 3;
char hexaKeys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

byte rowPins[ROWS] = { 39, 40, 41, 42 };// Conectar el keypad ROW0, ROW1, ROW2 and ROW3 to these Arduino pins.
byte colPins[COLS] = { 43, 44, 45 }; 
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); // variable_mapa de la matriz
unsigned long currentMillis = millis();

//                         INICIALIZAR EL VOID SETUP O CONFIGURACIONES DE INICIO
/* En este apartado se añaden todos los comandos que se ejecutaran en el arranque de la tarjeta es decir estos comandos
solo se ejecutan una vez */
 
void setup() {
 
 Serial.begin(9600); // asignar la frecuencia de comunicacion entre el arduino mega y el puerto Serial en 9600 Baudios
 lcd.init();         // inicializar la LCD 20x4
 lcd.backlight();    // Encendemos el led que da el brillo en la pantalla LCD
 Wire.begin();       // inicializar el protocolo de comunicacion I2c entre el RTC, LCD 20x4 y el Arduino Mega
 rtc.begin();        // Inicializar la rtc o reloj en tiempo real
 ph.begin();         // Inicializar libreria sensor de PH
 ec.begin();         // inicializar libreria Sensor de CE
 sensorDS18B20.begin(); //Inicializar libreria sensor de temperatura sensorDS18B20

 /* Inicio de funciones 1. AjusteTemp(): Esta funcion toma el primer valor de temperatura y humedad para almacenarlo y saber en que 
 direccion debe actuar la activacion del extractor con respecto a la histeresis. ej si T=35°C pero Tbase=20°C y Hist=1°C entonces el extractor
 debe actuar hasta que llegue a 20°C - 1°C en este instante se apaga y vuelve a encender hasta que sea 20°C + 1°C */

 AjusteTemp();
 AjusteNivel();

 /* establecer el estado de las variables antes mencionadas definir si estas son entradas o salidas (INPUT- OUTPUT) 
respectivamente adicionalmente el nivel logico en el cual inician alto 5V o bajo 0V (HIGH , LOW). 
Cabe resaltar que los interruptores que se usaron (Reles) Son de logica negativa por ende 
El estado HIGH equivale a 0V y el estado LOW equivale a 5V                                                                  */
 
 pinMode(Bomba1,OUTPUT); 
 pinMode(Bomba2,OUTPUT);
 pinMode(Bomba3,OUTPUT);
 pinMode(Bomba4,OUTPUT);
 pinMode(Extractor,OUTPUT);
 digitalWrite(Bomba1, HIGH);
 digitalWrite(Bomba2, HIGH);
 digitalWrite(Bomba3, HIGH);
 digitalWrite(Bomba4, HIGH);

/* 1. La funcion menu_vienvenida() es una funcion la cual se encarga que al inicializar el arduino
este permite visualizar en la pantalla LCD informacion del proyecto esto solo ocurre una vez y es 
cada cuanto el arduino se inicia. 
2. La funcion Reloj() cumple la razon de inicializar el RTC de la siguiente manera T = rtc.getTime() 
donde T es una variable asimilada al Tiempo de esta forma se adquiere el valor de tiempo y fecha 
que tiene el reloj en este caso RTC, esta fecha se vera modificada de dos formas la primera puede ser intencional
al modificar la linea de codigo anteriormente mencionada que modifica fecha y hora o la segunda la cual
se ve afectada la fecha por el deterioro de la bateria que este dispositivo contiene
3. La funcion memoria() realiza la operacion de adquirir cualquier dato almacenado en la memoria EEPROM en esta primera instancia
del programa en el Void Setup solo se limita a pedir todos los datos almacenados en la memoria */

 menu_vienvenida();
 Reloj();
 memoria();

/*                        Configuracion de Threads correspondientes al controlador 1 

la Estructura de este bloque de programacion esta definido por la libreria pero consta de emparejar El Hilo el cual
al inicio se definio ThreadPh proceder a inicializar una funcion Ej Sensor_Ph (La cual se encarga de medir el nivel de Ph) 
por medio de la palabra onRUN  luego asignar el valor en milisegundos que esta funcion tomara datos en este Ejemplo
la funcion de tomar el valor de ph en la solucion sera ejecutada cada 500ms igual a 0.5 segundos lo que equivale a 120 lecturas por minuto     
De esta forma se tiene mejor control en el momento de realizar subprocesos en el codigo asiendo alucion a llamados de funcion en tiempos 
especificos  */

 Threadph->onRun(Sensor_Ph);                 // Sensor_Ph: funcion Lectura sensor de ph
   Threadph->setInterval(500);               
 Threadlcd.onRun(MSNinicio);                 // MSNinicio: funcion Mensaje de inicio
   Threadlcd.setInterval(5000);
 Threadce.onRun(Sensor_CE);                  // Sensor_CE: funcion Lectura sensor de conductivida electrica
   Threadce.setInterval(500);            
 Threadcorrph.onRun(correccion_ph);          // correccion_ph: funcion Correccion de ph
   Threadcorrph.setInterval(900);
 Threadcorrce.onRun(correccion_ce);          // correccion_ce: funcion Correccion de Conductividad electrica
   Threadcorrce.setInterval(300);

 //ThreadCalibracion.onRun(calibracion);
  // ThreadCalibracion.setInterval(1000);
 ThreadRiegos.onRun(riegos);                 // riegos: funcion Revision de tiempos de riego
   ThreadRiegos.setInterval(1000);
 ThreadTempsoluc.onRun(readTemperature);     // readTemperature: funcion leer temperatura de la solucion nutritiva
   ThreadTempsoluc.setInterval(250);

/* En esta instancia se se añadan los hilos o subprocesos al control especifico como se habia mencionado
el controll1 se encarga de todas las funciones hilos o subprocesos que intervienen en la solucion nutritiva
medicion de ph, ce y temperatura de la solucion, ajuste de las mismas con respecto a los valores de SetPoint y control de riego.               */
 
 controll.add(Threadph);
 controll.add(&Threadlcd);
 controll.add(&Threadce);
 controll.add(&Threadcorrph);
 controll.add(&Threadcorrce);
 //controll.add(&ThreadCalibracion);
 controll.add(&ThreadRiegos);
 controll.add(&ThreadTempsoluc);
//_________________________________________________________________________________________________________________________________________

 /*                  Configuracion de Threads correspondientes al controlador 2                                                                           */
 ThreadLCDBomba->onRun(menu_proceso);   // menu_proceso: Funcion que enseña datos de variables mientras que se encuentra en riego el proceso
   ThreadLCDBomba->setInterval(500);
 controll2.add(ThreadLCDBomba);
//_________________________________________________________________________________________________________________________________________

/*                   Configuracion de Threads correspondientes al controlador 3                                                                      */
ThreadTemHumedad->onRun(Temperatura_Ambiente);  //Temperatura_Ambiente: Funcion encargada de medir y modificar temperatura y humedad ambiente
   ThreadTemHumedad->setInterval(1300);
ThreadLuz.onRun(SensorLuz);                     //SensorLuz: Funcion encargada de realizar las mediciones de luminocidad en el cultivo 
   ThreadLuz.setInterval(2000);
Threadnivel.onRun(nivel);                       //nivel: Funcion encargada de tomar datos del nivel que posee la solucion nutritiva en el deposito
   Threadnivel.setInterval(100);
   
 controll3.add(ThreadTemHumedad);
 controll3.add(&ThreadLuz);
 controll3.add(&Threadnivel);
//_________________________________________________________________________________________________________________________________________
}    // Final Del VOID SETUP o Configuraciones Iniciales       

/*                               Inicio de Void Loop o Inicio de funcion ciclico
El void Loop es la funcion principal es el apartado del programa donde se añaden los comandos que se ejecutaran mientras
la placa Arduino este habilitada. Algo particular de este programa es la nula utilizacion de Delays las cuales tienen como
funcion detener el proceso para ejecutar una accion, en pocas palabras inhabilita otras funciones

en contraste se usa la opcion de funcion Treads o hilos los cuales hacen del proceso una similitud a Procesos que se ejecutan en 
paralelo permitiendo ejecutar multiples subprocesos en simultanea al ser llamados segun el tiempo en ms que se le asigna al Hilo.                          */

void loop() {
/* La funcion memoria() en el void setup tenia la funcion de recolectar los datos almacenados en la memoria EEPROM en cambio
en la funcion Void Loop cumple la tarea de verificar en cada momento si los datos de la memoria se ven afectados para sobreescribir
en la misma. Estos valores estan asignados en 5 grupos las mismas especies de planta (Lechuga crespa, Lechuga Romana, Albahaca, Espinaca y Acelga)
entonces cada ves que entro al apartado de seleccion de cultivo esta en paralelo funcionando memoria, si modifico el tipo de cultivo cambiandolo
automaticamente actua la funcion memoria sobreescribiendo en tiempo real los datos correspondientes al cultivo seleccionado

Esta caracteristica es el valor agregado que se le da al proyecto permitiendo que cualquier persona sin ser especializada en la parte
agronomica con tan solo definir que decea cultivar este se adapte automaticamente modificando los valores de Setpoint almacenados en la 
memoria EEPROM del Arduino Mega                                                                                                                     */
    memoria();
//_________________________________________________________________________________________________________________________________________
z=0;
y=0;
p=0;
d=0;
seleccion();
Reloj();
tecla=customKeypad.getKey();
Serial.print(rtc.getTimeStr());
if ((T.hour>=h1_c1) &&(T.hour<=19))   {
   controll.run();
   controll3.run();
Reloj();
Serial.print(rtc.getTimeStr());
if ( b>=1){                    
  activar_bomba();
  controll2.run(); // Mostrar en pantalla LCD mensaje para no manipular el teclado
}

else {
  apagarBombas();
}
Serial.println(c);
Serial.println(d);
 }
 else{
 sleep();
 }

}




/****************************************************ZONE 1 SENSORES SOLUCION NUTRITIVA******************************************************************/

void ZoneSense1() {
  while (y<1)
  {
        tecla=customKeypad.getKey(); //CHOOSE SENSOR 
          if(tecla =='1'){ 
             for(i=0; i < 15; i++){
               
                  Sensor_Ph();
                  Sensor_CE();
                  lcd.clear();
                  lcd.setCursor (0, 0);
                  lcd.print("PH          =");
                  lcd.setCursor (15, 0);
                  lcd.print(phValue);
                  lcd.setCursor (0, 1);
                  lcd.print("TEMPERATURA =");
                  lcd.setCursor (15, 1);
                  lcd.print(sensorDS18B20.getTempCByIndex(0));
                  lcd.setCursor (0, 2);
                  lcd.print("CE          =");
                  lcd.setCursor (15, 2);
                  lcd.print(ecValue);
                  delay(200);
               }
           y=1;
          }    
          if(tecla =='2'){ 
          for(i=0; i < 15; i++){
          lcd.clear();
          lcd.setCursor (0, 0);
          lcd.print("CICLOS DE RIEGO=");
          lcd.setCursor (16, 0);
          lcd.print(R);
          lcd.setCursor (0, 1);
          lcd.print("FECHA");
          lcd.setCursor(1,2);
          lcd.print(rtc.getDOWStr());
          lcd.print(' ');
          lcd.print(rtc.getDateStr());
          lcd.setCursor(4,3);
          lcd.print(rtc.getTimeStr());
          }
             y=1;
             }
}
}



/**********************************************ZONE 2 SENSORES DEL INVERNADERO*************************************************************************/

/**********************************************ZONE 2 SENSORES DEL INVERNADERO*************************************************************************/

void ZoneSense2() {
 while(y<1)  
        {
        tecla=customKeypad.getKey(); //CHOOSE SENSOR 
          if(tecla =='1'){
             for(i=0; i < 15; i++){
                float h = dht.readHumidity(); //Leemos la Humedad
                float t = dht.readTemperature(); //Leemos la temperatura en grados Celsius
          lcd.clear();
          lcd.setCursor (0, 0);
          lcd.print("TEMPERATURA =");
          lcd.setCursor (15, 1);
          lcd.print(t);
          lcd.setCursor (0, 1);
          lcd.print("% HUMEDAD =");
          lcd.setCursor (15, 0);
          lcd.print(h);
       
          lcd.setCursor (0, 3);
          lcd.print("CAUDAL =");
          delay(200);
            }
          y=1;
          z=1;
          }
          //OPCION 2 MENU SENSORES DEL INVERNADERO//
          if(tecla =='2'){  
              for(i=0; i < 15; i++){
          lcd.clear();
          lcd.setCursor (0, 0);
          lcd.print("luz 1");
          lcd.setCursor (10, 0);
          lcd.print(dato);
          lcd.setCursor (0, 1);
          lcd.print("luz 2");
          lcd.setCursor (10, 1);
          lcd.print(dato1);
          delay(100);
             }     
           y=1;
           z=1;
                }
          
}
}

void zoneSense3(){
  digitalWrite(Bomba1, HIGH);
    digitalWrite(Bomba2, HIGH);
     digitalWrite(Bomba3, HIGH);
     digitalWrite(Bomba4, HIGH);
  while(y<1)  
        {
        tecla=customKeypad.getKey(); //CHOOSE SENSOR 
          if(tecla =='1'){
             lcd.clear();
             lcd.setCursor(0,0);
             lcd.print("Se detendra el proceso!");
             lcd.setCursor(0,1);
             lcd.print("Continuar  *");
             lcd.setCursor(0,2);
             lcd.print("Salir      0");
             
          }
             if (tecla == '*')
             {
               lcd.clear();
             lcd.setCursor(0,0);
             lcd.print("  Proceso detenido ");
             lcd.setCursor(0,1);
             lcd.print(" Realice los pasos ");
             lcd.setCursor(0,2);
             lcd.print(" Del Manual calibr ");
             ph.calibration(voltage,temperature);   
             ec.calibration(voltage1,temperature);   
             }

             if (tecla == '0'){
               y=1;
             }
          
     if (tecla=='2'){
      f=1; 
      nivel();
      //Pregunto por el nivel si el nivel es bajo encender bomba 3 y apagar bomba 4 siempre y cuando compruebe pulsando #
       lcd.clear();
              lcd.print("  Proceso detenido ");
             lcd.setCursor(0,1);
             lcd.print(" Realice los pasos  ");
             lcd.setCursor(0,2);
             lcd.print("Del Manual C de sol ");  
             digitalWrite(Bomba4, LOW);
            
     }
             if (tecla == '#'){
             digitalWrite(Bomba4, LOW);
             }
           
      if (tecla == '0'){
               digitalWrite(Bomba4, HIGH);
               digitalWrite(Bomba3, LOW);
               y=1;
             }
        }
}


  
